// document.getElementById('loginForm').addEventListener('submit', function (event) {
//     event.preventDefault();

//     // Lấy giá trị từ các trường input
//     var username = document.getElementById('username').value;
//     var password = document.getElementById('password').value;

//     // Kiểm tra thông tin đăng nhập
//     if (username === 'admin' && password === 'admin123') {
//         document.getElementById('message').textContent = 'Đăng nhập thành công';
//         window.location.href = "/src/homepage.html"
//     } else {
//         document.getElementById('message').textContent = 'Tên đăng nhập hoặc mật khẩu không đúng';
//     }
// });

async function fethHandler() {

    try {
      let response = await fetch('https://jsonplaceholder.typicode.com/posts');
      let posts = await response.json();
      if(!response.ok) {
         const error = new Error('An Error Occured');
         error.details = posts;
         throw error;
      }
      console.log(posts);
    } catch(e) {
      console.log(e.message); // An Error Occurred
      console.log(e.details); // prints response got from server
    }
  }
  
  fethHandler();